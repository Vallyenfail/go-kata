package repo

import (
	"errors"
	"sync"

	"gitlab.com/Vallyenfail/go-kata/module4/webserver/selenium/models"
)

type VacancyStorager interface {
	Create(dto models.Vacancy) error
	GetByID(id int) (models.Vacancy, error)
	GetList() ([]models.Vacancy, error)
	Delete(id int) error
}

type VacancyStorage struct {
	data                 []*models.Vacancy
	primaryKeyIDx        map[int]*models.Vacancy
	autoIncrementCounter int
	sync.Mutex
}

func NewVacancyStorage() *VacancyStorage {
	return &VacancyStorage{
		data:                 make([]*models.Vacancy, 0),
		primaryKeyIDx:        make(map[int]*models.Vacancy),
		autoIncrementCounter: 0,
	}
}

func (v *VacancyStorage) Create(dto models.Vacancy) error {
	v.Lock()
	defer v.Unlock()
	dto.ID = v.autoIncrementCounter
	v.data = append(v.data, &dto)
	v.primaryKeyIDx[dto.ID] = &dto
	v.autoIncrementCounter++

	return nil
}

func (v *VacancyStorage) GetByID(id int) (models.Vacancy, error) {
	if val, ok := v.primaryKeyIDx[id]; ok {
		return *val, nil
	}

	return models.Vacancy{}, errors.New("not found")
}

func (v *VacancyStorage) GetList() ([]models.Vacancy, error) {
	var list []models.Vacancy
	for _, v := range v.data {
		list = append(list, *v)
	}

	return list, nil
}

func (v *VacancyStorage) Delete(id int) error {
	if _, ok := v.primaryKeyIDx[id]; ok {
		v.Lock()
		defer v.Unlock()
		switch {
		default:
			v.data = append(v.data[:id], v.data[id:]...)
		case id == 0:
			v.data = v.data[1:]
		case id == len(v.data)-1:
			v.data = v.data[:len(v.data)-1]
		}
		delete(v.primaryKeyIDx, id)
		return nil
	}

	return errors.New("not found")
}
