package models

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    vacancy, err := UnmarshalVacancy(bytes)
//    bytes, err = vacancy.Marshal()

import "encoding/json"

func UnmarshalVacancy(data []byte) (Vacancy, error) {
	var r Vacancy
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Vacancy) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Vacancy struct {
	ID                 int                `json:"id"`
	Context            string             `json:"@context"`
	Type               string             `json:"@type"`
	DatePosted         string             `json:"datePosted"`
	Title              string             `json:"title"`
	Description        string             `json:"description"`
	ValidThrough       string             `json:"validThrough"`
	HiringOrganization HiringOrganization `json:"hiringOrganization"`
	EmploymentType     string             `json:"employmentType"`
}

type HiringOrganization struct {
	Name   string `json:"name"`
	Logo   string `json:"logo"`
	SameAs string `json:"sameAs"`
}
