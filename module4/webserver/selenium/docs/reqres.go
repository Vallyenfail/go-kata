package docs

import "gitlab.com/Vallyenfail/go-kata/module4/webserver/selenium/models"

//nolint: all

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /search vacancy vacancyCreateRequest
// Найти вакансии.
// responses:
// 200: vacancyCreateResponse

// swagger:parameters vacancyCreateRequest
type vacancyCreateRequest struct {
	//in:body
	Body models.Search
}

// swagger:response vacancyCreateResponse
type vacancyCreateResponse struct {
}

// swagger:route GET /vacancy/{id} vacancy vacancyGetByIDRequest
// Получить вакансию по ID.
// responses:
// 200: vacancyGetByIDResponse

// swagger:parameters vacancyGetByIDRequest
type vacancyGetByIDRequest struct {
	//in:path
	ID string `json:"id"`
}

// swagger:response vacancyGetByIDResponse
type vacancyGetByIDResponse struct {
	//in:body
	Body models.Vacancy
}

// swagger:route GET /vacancies vacancy vacancyGetListRequest
// Получить список вакансий.
// responses:
// 200: vacancyGetListResponse

// swagger:parameters vacancyGetListRequest

// swagger:response vacancyGetListResponse
type vacancyGetListResponse struct {
	//in:body
	Body []models.Vacancy
}

// swagger:route DELETE /vacancy/{id} vacancy vacancyDeleteRequest
// Удалить вакансию по ID.
// responses:
// 200: vacancyDeleteResponse

// swagger:parameters vacancyDeleteRequest
type vacancyDeleteRequest struct {
	//in:path
	ID string `json:"id"`
}

// swagger:response vacancyDeleteResponse
type vacancyDeleteResponse struct {
}
