package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/Vallyenfail/go-kata/module4/webserver/selenium/models"
	"gitlab.com/Vallyenfail/go-kata/module4/webserver/selenium/repo"
)

type VacancyController struct {
	storage repo.VacancyStorager
}

func NewVacancyController() *VacancyController {
	return &VacancyController{storage: repo.NewVacancyStorage()}
}

func (v *VacancyController) VacancySearch(w http.ResponseWriter, r *http.Request) {
	var search models.Search
	err := json.NewDecoder(r.Body).Decode(&search) //get the query
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	vacanciesList := findVacancies(search.Query) // find links of vacancies

	for _, link := range vacanciesList { // get vacancies JSONs and add it to the data storage
		var vacancy models.Vacancy
		vacJSON := getVacancyJSON(link)
		err := json.Unmarshal([]byte(vacJSON), &vacancy)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		_ = v.storage.Create(vacancy)
	}

}

func (v *VacancyController) VacancyGetByID(w http.ResponseWriter, r *http.Request) {
	var (
		vacancy      models.Vacancy
		vacancyIDRaw string
		vacancyID    int
		err          error
	)

	vacancyIDRaw = chi.URLParam(r, "id")

	vacancyID, err = strconv.Atoi(vacancyIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	vacancy, err = v.storage.GetByID(vacancyID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(vacancy)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyController) VacancyDelete(w http.ResponseWriter, r *http.Request) {
	var (
		vacID    int
		vacIDRaw string
		err      error
	)

	vacIDRaw = chi.URLParam(r, "id")

	vacID, err = strconv.Atoi(vacIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = v.storage.Delete(vacID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}
}

func (v *VacancyController) VacancyGetList(w http.ResponseWriter, r *http.Request) {
	var vacList []models.Vacancy
	var err error

	vacList, err = v.storage.GetList()

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(vacList)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
