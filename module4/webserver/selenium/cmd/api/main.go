package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/Vallyenfail/go-kata/module4/webserver/selenium/handlers"
	"gitlab.com/Vallyenfail/go-kata/module4/webserver/selenium/router"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func main() {
	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	vacancyController := handlers.NewVacancyController()
	r.Post("/search", vacancyController.VacancySearch)
	r.Get("/vacancy/{id}", vacancyController.VacancyGetByID)
	r.Get("/vacancies", vacancyController.VacancyGetList)
	r.Delete("/vacancy/{id}", vacancyController.VacancyDelete)

	//SwaggerUI
	r.Get("/swagger", router.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./module4/webserver/selenium/public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:         port,
		Handler:      r,
		ReadTimeout:  time.Second * 10,
		WriteTimeout: time.Second * 10,
	}

	// Webserver
	go func() {
		log.Printf(fmt.Sprintf("server started on port %s", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// waiting for the signal to terminate
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit
	log.Println("Shutdown Server...")

	// Timeout for finishing the job
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
