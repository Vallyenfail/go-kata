package service

import (
	"bufio"
	"io"
	"net/http"
	"os"
)

type Servicer interface {
	Upload(r *http.Request) error
	GetFile(filePath string) ([]byte, error)
}

type Service struct {
}

func (s *Service) Upload(r *http.Request) error {
	reader := bufio.NewReader(r.Body)
	err := os.Chdir("./module4/webserver/http/homework/public")
	if err != nil {
		return err
	}
	file, _ := os.Create("filename.txt")
	defer func(file *os.File) {
		_ = file.Close()
	}(file)
	_, _ = io.Copy(file, reader)

	return nil
}

func (s *Service) GetFile(filePath string) ([]byte, error) {
	file, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	return file, nil
}
