package repository

import (
	"gitlab.com/Vallyenfail/go-kata/module4/webserver/http/homework/user/models"
)

type UserStorager interface {
	GetList() ([]models.User, error)
	GetByID(id int) (models.User, error)
}

type UserStorage struct {
	data []*models.User
}

func NewUserStorage() *UserStorage {
	return &UserStorage{data: []*models.User{&models.User{
		ID:       0,
		Name:     "andie",
		Adress:   "Moscow",
		Username: "andiebrody",
	}, &models.User{
		ID:       1,
		Name:     "Muscle",
		Adress:   "Washington",
		Username: "Dc",
	}}}
}

func (u *UserStorage) GetList() ([]models.User, error) {
	var users []models.User

	for _, user := range u.data {
		users = append(users, *user)
	}
	return users, nil
}

func (u *UserStorage) GetByID(id int) (models.User, error) {
	return *u.data[id], nil
}
