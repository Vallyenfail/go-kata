package models

type User struct {
	ID       int    `json:"id,omitempty"`
	Name     string `json:"name,omitempty"`
	Adress   string `json:"adress,omitempty"`
	Username string `json:"username,omitempty"`
}
