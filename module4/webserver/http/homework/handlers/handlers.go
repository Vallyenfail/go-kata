package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/Vallyenfail/go-kata/module4/webserver/http/homework/service"
	"gitlab.com/Vallyenfail/go-kata/module4/webserver/http/homework/user/models"
	"gitlab.com/Vallyenfail/go-kata/module4/webserver/http/homework/user/repository"
)

type UserController struct {
	storage repository.UserStorager
}

type Controller struct {
	service service.Servicer
}

func NewUserController() *UserController {
	return &UserController{storage: repository.NewUserStorage()}
}

func NewController() *Controller {
	return &Controller{service: &service.Service{}}
}

func Hello(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte(`{"message": "Hello, dude"}`))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) Upload(w http.ResponseWriter, r *http.Request) {
	_ = c.service.Upload(r)

	w.WriteHeader(http.StatusOK)
}

func (c *Controller) GetFile(w http.ResponseWriter, r *http.Request) {
	fileName := chi.URLParam(r, "filename")
	filePath := "./module4/webserver/http/homework/public/" + fileName

	file, _ := c.service.GetFile(filePath)
	_, err := w.Write(file)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (u *UserController) GetUsers(w http.ResponseWriter, r *http.Request) {
	var userList []models.User
	var err error

	userList, _ = u.storage.GetList()

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(userList)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserController) GetUserByID(w http.ResponseWriter, r *http.Request) {
	var soloUser models.User
	var err error

	idRaw := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	soloUser, _ = u.storage.GetByID(id)

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(soloUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
