package repo

import (
	"reflect"
	"testing"

	"gitlab.com/Vallyenfail/go-kata/module4/webserver/swagger/models"
)

//nolint:all

var (
	PetTest = models.Pet{
		ID: 1,
		Category: models.Category{
			ID:   0,
			Name: "string",
		},
		Name:      "Mosey",
		PhotoUrls: nil,
		Tags:      nil,
		Status:    "",
	}

	PetTestUpdate = models.Pet{
		ID: 2,
		Category: models.Category{
			ID:   0,
			Name: "string",
		},
		Name:      "Lennie",
		PhotoUrls: nil,
		Tags:      nil,
		Status:    "",
	}

	StoreTest = models.Store{
		ID:       1,
		PetID:    0,
		Quantity: 20,
		ShipDate: "",
		Status:   "",
		Complete: false,
	}

	UserTest = models.User{
		ID:         1,
		Username:   "Danny",
		FirstName:  "Daniel",
		LastName:   "Lego",
		Email:      "string",
		Password:   "string",
		Phone:      "string",
		UserStatus: 0,
	}

	UserTestUpdate = models.User{
		ID:         1,
		Username:   "Danny",
		FirstName:  "Marty",
		LastName:   "Farwell",
		Email:      "string",
		Password:   "string",
		Phone:      "string",
		UserStatus: 0,
	}
)

func TestPetStorage_Create(t *testing.T) {
	type fields struct {
		data               []*models.Pet
		primaryKeyIDx      map[int]*models.Pet
		autoIncrementCount int
	}
	type args struct {
		pet models.Pet
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.Pet
	}{
		{
			name: "first test",
			args: args{pet: PetTest},
			fields: fields{
				data:               make([]*models.Pet, 0, 13),
				primaryKeyIDx:      make(map[int]*models.Pet, 13),
				autoIncrementCount: 1,
			},
			want: PetTest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if got := p.Create(tt.args.pet); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_Delete(t *testing.T) {
	type fields struct {
		data               []*models.Pet
		primaryKeyIDx      map[int]*models.Pet
		autoIncrementCount int
	}
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "second test",
			args: args{petID: 1},
			fields: fields{
				data:               []*models.Pet{&PetTest},
				primaryKeyIDx:      map[int]*models.Pet{1: &PetTest},
				autoIncrementCount: 1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := p.Delete(tt.args.petID); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPetStorage_GetByID(t *testing.T) {
	type fields struct {
		data               []*models.Pet
		primaryKeyIDx      map[int]*models.Pet
		autoIncrementCount int
	}
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    models.Pet
		wantErr bool
	}{
		{
			name: "third test",
			fields: fields{
				data:               []*models.Pet{&PetTest},
				primaryKeyIDx:      map[int]*models.Pet{1: &PetTest},
				autoIncrementCount: 1,
			},
			args: args{petID: 1},
			want: PetTest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := p.GetByID(tt.args.petID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_GetList(t *testing.T) {
	type fields struct {
		data               []*models.Pet
		primaryKeyIDx      map[int]*models.Pet
		autoIncrementCount int
	}
	tests := []struct {
		name   string
		fields fields
		want   []models.Pet
	}{
		{
			name: "fourth test",
			fields: fields{
				data:               []*models.Pet{&PetTest, &PetTestUpdate},
				primaryKeyIDx:      map[int]*models.Pet{1: &PetTest, 2: &PetTestUpdate},
				autoIncrementCount: 1,
			},
			want: []models.Pet{PetTest, PetTestUpdate},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if got := p.GetList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_Update(t *testing.T) {
	type fields struct {
		data               []*models.Pet
		primaryKeyIDx      map[int]*models.Pet
		autoIncrementCount int
	}
	type args struct {
		pet models.Pet
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    models.Pet
		wantErr bool
	}{
		{
			name: "fifth test",
			fields: fields{
				data:               []*models.Pet{&PetTest, &PetTestUpdate},
				primaryKeyIDx:      map[int]*models.Pet{1: &PetTest, 2: &PetTestUpdate},
				autoIncrementCount: 1,
			},
			args: args{PetTestUpdate},
			want: PetTestUpdate,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := p.Update(tt.args.pet)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_Create(t *testing.T) {
	type fields struct {
		data               []*models.Store
		primaryKeyIDx      map[int]*models.Store
		autoIncrementCount int
	}
	type args struct {
		order models.Store
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.Store
	}{
		{name: "First store test",
			fields: fields{
				data:               make([]*models.Store, 0, 13),
				primaryKeyIDx:      make(map[int]*models.Store, 0),
				autoIncrementCount: 1,
			},
			args: args{StoreTest},
			want: StoreTest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if got := s.Create(tt.args.order); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_DeleteById(t *testing.T) {
	type fields struct {
		data               []*models.Store
		primaryKeyIDx      map[int]*models.Store
		autoIncrementCount int
	}
	type args struct {
		orderID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{name: "Second store test",
			fields: fields{
				data:               []*models.Store{&StoreTest},
				primaryKeyIDx:      map[int]*models.Store{1: &StoreTest},
				autoIncrementCount: 2,
			},
			args: args{orderID: 1},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := s.DeleteById(tt.args.orderID); (err != nil) != tt.wantErr {
				t.Errorf("DeleteById() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestStoreStorage_GetByID(t *testing.T) {
	type fields struct {
		data               []*models.Store
		primaryKeyIDx      map[int]*models.Store
		autoIncrementCount int
	}
	type args struct {
		orderID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    models.Store
		wantErr bool
	}{
		{
			name: "Third store test",
			fields: fields{
				data:               []*models.Store{&StoreTest},
				primaryKeyIDx:      map[int]*models.Store{1: &StoreTest},
				autoIncrementCount: 2,
			},
			args: args{orderID: 1},
			want: StoreTest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := s.GetByID(tt.args.orderID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_GetList(t *testing.T) {
	type fields struct {
		data               []*models.Store
		primaryKeyIDx      map[int]*models.Store
		autoIncrementCount int
	}
	tests := []struct {
		name   string
		fields fields
		want   []models.Store
	}{
		{
			name: "Fourth store test",
			fields: fields{
				data:               []*models.Store{&StoreTest, &StoreTest},
				primaryKeyIDx:      map[int]*models.Store{1: &StoreTest, 2: &StoreTest},
				autoIncrementCount: 3,
			},
			want: []models.Store{StoreTest, StoreTest},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &StoreStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if got := s.GetList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_Create(t *testing.T) {
	type fields struct {
		data               []*models.User
		primaryKeyIDx      map[int]*models.User
		autoIncrementCount int
	}
	type args struct {
		user models.User
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.User
	}{
		{
			name: "First user test",
			fields: fields{
				data:               make([]*models.User, 0, 13),
				primaryKeyIDx:      make(map[int]*models.User, 0),
				autoIncrementCount: 1,
			},
			args: args{UserTest},
			want: UserTest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if got := u.Create(tt.args.user); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_Delete(t *testing.T) {
	type fields struct {
		data               []*models.User
		primaryKeyIDx      map[int]*models.User
		autoIncrementCount int
	}
	type args struct {
		user models.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Second user test",
			fields: fields{
				data:               []*models.User{&UserTest, &UserTestUpdate},
				primaryKeyIDx:      map[int]*models.User{1: &UserTest, 2: &UserTestUpdate},
				autoIncrementCount: 3,
			},
			args: args{user: UserTest},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := u.Delete(tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserStorage_GetByName(t *testing.T) {
	type fields struct {
		data               []*models.User
		primaryKeyIDx      map[int]*models.User
		autoIncrementCount int
	}
	type args struct {
		username string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    models.User
		wantErr bool
	}{
		{
			name: "Third user test",
			fields: fields{
				data:               []*models.User{&UserTest, &UserTestUpdate},
				primaryKeyIDx:      map[int]*models.User{1: &UserTest, 2: &UserTestUpdate},
				autoIncrementCount: 3,
			},
			args: args{username: "Danny"},
			want: UserTest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := u.GetByName(tt.args.username)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByName() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_Update(t *testing.T) {
	type fields struct {
		data               []*models.User
		primaryKeyIDx      map[int]*models.User
		autoIncrementCount int
	}
	type args struct {
		user models.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    models.User
		wantErr bool
	}{
		{
			name: "Fourth user test",
			fields: fields{
				data:               []*models.User{&UserTest},
				primaryKeyIDx:      map[int]*models.User{1: &UserTest},
				autoIncrementCount: 2,
			},
			args: args{UserTestUpdate},
			want: UserTestUpdate,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := u.Update(tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
		})
	}
}
