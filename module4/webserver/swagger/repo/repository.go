package repo

import (
	"fmt"
	"sync"

	"gitlab.com/Vallyenfail/go-kata/module4/webserver/swagger/models"
)

type PetStorager interface {
	Create(pet models.Pet) models.Pet
	Update(pet models.Pet) (models.Pet, error)
	Delete(petID int) error
	GetByID(petID int) (models.Pet, error)
	GetList() []models.Pet
}

type UserStorager interface {
	Create(user models.User) models.User
	GetByName(username string) (models.User, error)
	Delete(user models.User) error
	Update(user models.User) (models.User, error)
}

type StoreStorager interface {
	Create(order models.Store) models.Store
	DeleteById(orderID int) error
	GetByID(orderID int) (models.Store, error)
	GetList() []models.Store
}

type PetStorage struct {
	data               []*models.Pet
	primaryKeyIDx      map[int]*models.Pet
	autoIncrementCount int
	sync.Mutex
}

func NewPetStorage() *PetStorage {
	return &PetStorage{
		data:               make([]*models.Pet, 0, 13),
		primaryKeyIDx:      make(map[int]*models.Pet, 13),
		autoIncrementCount: 1,
	}
}

type UserStorage struct {
	data               []*models.User
	primaryKeyIDx      map[int]*models.User
	autoIncrementCount int
	sync.Mutex
}

func NewUserStorage() *UserStorage {
	return &UserStorage{
		data:               make([]*models.User, 0, 13),
		primaryKeyIDx:      make(map[int]*models.User, 13),
		autoIncrementCount: 1,
	}
}

type StoreStorage struct {
	data               []*models.Store
	primaryKeyIDx      map[int]*models.Store
	autoIncrementCount int
	sync.Mutex
}

func NewStoreStorage() *StoreStorage {
	return &StoreStorage{
		data:               make([]*models.Store, 0, 13),
		primaryKeyIDx:      make(map[int]*models.Store, 13),
		autoIncrementCount: 1,
	}
}

func (u *UserStorage) Create(user models.User) models.User {
	u.Lock()
	defer u.Unlock()
	user.ID = u.autoIncrementCount
	u.autoIncrementCount++
	u.primaryKeyIDx[user.ID] = &user
	u.data = append(u.data, &user)

	return user
}

func (u *UserStorage) GetByName(username string) (models.User, error) {
	for _, user := range u.data {
		if user.Username == username {
			return *user, nil
		}
	}

	return models.User{}, fmt.Errorf("user not found")
}

func (u *UserStorage) Delete(user models.User) error {
	if _, ok := u.primaryKeyIDx[user.ID]; ok {
		u.Lock()
		defer u.Unlock()
		switch {
		default:
			u.data = append(u.data[:user.ID-1], u.data[user.ID:]...)
		case user.ID-1 == 0:
			u.data = u.data[1:]
		case user.ID-1 == len(u.data)-1:
			u.data = u.data[:len(u.data)-1]
		}
		delete(u.primaryKeyIDx, user.ID)
		return nil
	}

	return fmt.Errorf("user not found")
}

func (u *UserStorage) Update(user models.User) (models.User, error) {
	if _, ok := u.primaryKeyIDx[user.ID]; ok {
		u.Lock()
		defer u.Unlock()
		u.data[user.ID-1] = &user
		u.primaryKeyIDx[user.ID] = &user
		return user, nil
	}

	return models.User{}, fmt.Errorf("user not found")
}

func (s *StoreStorage) Create(order models.Store) models.Store {
	s.Lock()
	defer s.Unlock()

	order.ID = s.autoIncrementCount
	s.autoIncrementCount++
	s.primaryKeyIDx[order.ID] = &order
	s.data = append(s.data, &order)

	return order
}

func (s *StoreStorage) DeleteById(orderID int) error {
	if _, ok := s.primaryKeyIDx[orderID]; ok {
		s.Lock()
		defer s.Unlock()
		switch {
		default:
			s.data = append(s.data[:orderID-1], s.data[orderID:]...)
		case orderID-1 == 0:
			s.data = s.data[1:]
		case orderID-1 == len(s.data)-1:
			s.data = s.data[:len(s.data)-1]
		}
		delete(s.primaryKeyIDx, orderID)
		return nil
	}

	return fmt.Errorf("not found")
}

func (s *StoreStorage) GetByID(orderID int) (models.Store, error) {
	if v, ok := s.primaryKeyIDx[orderID]; ok {
		return *v, nil
	}

	return models.Store{}, fmt.Errorf("not found")
}

func (s *StoreStorage) GetList() []models.Store {
	var stores []models.Store
	for _, store := range s.data {
		stores = append(stores, *store)
	}

	return stores
}

func (p *PetStorage) Create(pet models.Pet) models.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)

	return pet
}

func (p *PetStorage) Update(pet models.Pet) (models.Pet, error) {
	if _, ok := p.primaryKeyIDx[pet.ID]; ok {
		p.Lock()
		defer p.Unlock()
		p.data[pet.ID-1] = &pet
		p.primaryKeyIDx[pet.ID] = &pet
		return pet, nil
	}
	return models.Pet{}, fmt.Errorf("not found")
}

func (p *PetStorage) Delete(petID int) error {
	if _, ok := p.primaryKeyIDx[petID]; ok {
		p.Lock()
		defer p.Unlock()
		switch {
		default:
			p.data = append(p.data[:petID-1], p.data[petID:]...)
		case petID-1 == 0:
			p.data = p.data[1:]
		case petID-1 == len(p.data)-1:
			p.data = p.data[:len(p.data)-1]
		}
		delete(p.primaryKeyIDx, petID)
		return nil
	}

	return fmt.Errorf("not found")
}

func (p *PetStorage) GetByID(petID int) (models.Pet, error) {
	if v, ok := p.primaryKeyIDx[petID]; ok {
		return *v, nil
	}

	return models.Pet{}, fmt.Errorf("not found")
}

func (p *PetStorage) GetList() []models.Pet {
	var pets []models.Pet
	for _, pet := range p.data {
		pets = append(pets, *pet)
	}

	return pets
}
