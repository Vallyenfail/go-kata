package docs

import "gitlab.com/Vallyenfail/go-kata/module4/webserver/swagger/models"

//nolint:all

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// responses:
//   200: petAddResponse

// swagger:parameters petAddRequest
type petAddRequest struct {
	// in:body
	Body models.Pet
}

// swagger:response petAddResponse
type petAddResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route GET /pet/{id} pet petGetByIDRequest
// Получение питомца по id.
// responses:
//   200: petGetByIDResponse

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// ID of an item
	//
	// In: path
	ID string `json:"id"`
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route GET /pets pet petGetListRequest
// Получение списка питомцев.
// responses:
//	200: petGetListResponse

// swagger:parameters petGetListRequest

// swagger:response petGetListResponse
type petGetListResponse struct {
	// in:body
	Body []models.Pet
}

// swagger:route PUT /pet/update pet petUpdateRequest
// Обновить питомца.
// responses:
//	200: petUpdateResponse

// swagger:parameters petUpdateRequest
type petUpdateRequest struct {
	//in: body
	Body models.Pet
}

// swagger:response petUpdateResponse
type petUpdateResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route DELETE /pet/{id} pet petDeleteRequest
// Удалить питомца.
// responses:
//	200: petDeleteResponse

// swagger:parameters petDeleteRequest
type petDeleteRequest struct {
	//in: path
	ID string `json:"id"`
}

// swagger:response petDeleteResponse
type petDeleteResponse struct {
}

// swagger:route POST /user user userCreateRequest
// Добавить юзера.
// responses:
// 200: userCreateResponse

// swagger:parameters userCreateRequest
type userCreateRequest struct {
	//in: body
	Body models.User
}

// swagger:response userCreateResponse
type userCreateResponse struct {
	//in: body
	Body models.User
}

// swagger:route GET /user/{username} user userGetByNameRequest
// Получить юзера по username.
// responses:
//
//	200: userGetByNameResponse

// swagger:parameters userGetByNameRequest
type userGetByNameRequest struct {
	//in:path
	Username string `json:"username"`
}

// swagger:response userGetByNameResponse
type userGetByNameResponse struct {
	//in:body
	Body models.User
}

// swagger:route DELETE /user/delete user userDeleteRequest
// Удалить юзера.
// responses:
//
//	200: userDeleteResponse

// swagger:parameters userDeleteRequest
type userDeleteRequest struct {
	//in:body
	Body models.User
}

// swagger:response userDeleteResponse
type userDeleteResponse struct {
}

// swagger:route PUT /user/update user userUpdateRequest
// Обновить юзера.
// responses:
//
//	200: userUpdateResponse

// swagger:parameters userUpdateRequest
type userUpdateRequest struct {
	//in:body
	Body models.User
}

// swagger:response userUpdateResponse
type userUpdateResponse struct {
	//in:body
	Body models.User
}

// swagger:route POST /store/order store storeCreateRequest
// Добавить заказ.
// responses:
//	200: storeCreateResponse

// swagger:parameters storeCreateRequest
type storeCreateRequest struct {
	//in:body
	Body models.Store
}

// swagger:response storeCreateResponse
type storeCreateResponse struct {
	//in:body
	Body models.Store
}

// swagger:route GET /store/order/{id} store storeGetByIdRequest
// Получить заказ по айди.
// responses:
//	200: storeGetByIdResponse

// swagger:parameters storeGetByIdRequest
type storeGetByIdRequest struct {
	//in:path
	ID string `json:"id"`
}

// swagger:response storeGetByIdResponse
type storeGetByIdResponse struct {
	//in:body
	Body models.Store
}

// swagger:route Get /store/list store storeGetListRequest
// Получить список заказов.
// responses:
//	200: storeGetListResponse

// swagger:parameters storeGetListRequest

// swagger:response storeGetListResponse
type storeGetListResponse struct {
	//in:body
	Body []models.Store
}

// swagger:route DELETE /store/order/{id} store storeDeleteRequest
// Удалить заказ.
// responses:
//	200: storeDeleteResponse

// swagger:parameters storeDeleteRequest
type storeDeleteRequest struct {
	//in:path
	ID string `json:"id"`
}

// swagger:response storeDeleteResponse
type storeDeleteResponse struct {
}
