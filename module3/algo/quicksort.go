package algo

import "math/rand"

func QuickSort(arr []int) []int {
	if len(arr) <= 1 {
		return arr
	}

	median := arr[rand.Intn(len(arr))]

	low_part := make([]int, 0, len(arr))    // less than pivot
	high_part := make([]int, 0, len(arr))   // more than pivot
	middle_part := make([]int, 0, len(arr)) // equal to pivot

	for _, item := range arr {
		switch {
		case item < median:
			low_part = append(low_part, item)
		case item == median:
			middle_part = append(middle_part, item)
		case item > median:
			high_part = append(high_part, item)
		}
	}

	low_part = QuickSort(low_part)
	high_part = QuickSort(high_part)

	// unpacking middle part
	low_part = append(low_part, middle_part...)
	// unpacking high_part
	low_part = append(low_part, high_part...)

	return low_part
}
