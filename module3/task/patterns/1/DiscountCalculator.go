package main

import "fmt"

type PricingStrategy interface {
	Calculate(order Order) float64
}

type Order struct {
	product string
	price   float64
	number  int
}

type RegularPricing struct {
}

type SalePricing struct {
	saleStrategy SaleStrategy
}

type SaleStrategy struct {
	name    string
	percent float64
}

func (rp *RegularPricing) Calculate(order Order) float64 {
	fmt.Printf("Total cost of an regular order is : %f\n", order.price)
	return order.price
}

func (sp *SalePricing) Calculate(order Order) float64 {
	fmt.Printf("Total cost of an sale order is : %f\n", order.price*sp.saleStrategy.percent/100)
	return order.price * sp.saleStrategy.percent / 100
}

func main() {
	order := &Order{
		product: "bread",
		price:   200.0,
		number:  2,
	}

	saleStrategy := &SaleStrategy{
		name:    "New year's sale",
		percent: 15,
	}

	strategies := []PricingStrategy{
		&RegularPricing{},
		&SalePricing{saleStrategy: *saleStrategy},
	}

	for _, strategy := range strategies {
		strategy.Calculate(*order)
	}

}
