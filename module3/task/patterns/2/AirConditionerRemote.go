package main

import (
	"fmt"
	"log"
)

// AirConditioner is the interface that defines the methods for controlling the air conditioner
type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct{}

func (rac *RealAirConditioner) TurnOn() {
	fmt.Println("Turning on the air conditioner")
}

func (rac *RealAirConditioner) TurnOff() {
	fmt.Println("Turning off the air conditioner")
}

func (rac *RealAirConditioner) SetTemperature(temp int) {
	fmt.Printf("Setting air conditioner temperature to %d", temp)
}

type AirConditionerAdapter struct {
	RealAirConditioner *RealAirConditioner
}

func (aca *AirConditionerAdapter) TurnOn() {
	aca.RealAirConditioner.TurnOn()
}

func (aca *AirConditionerAdapter) TurnOff() {
	aca.RealAirConditioner.TurnOff()
}

func (aca *AirConditionerAdapter) SetTemperature(temp int) {
	aca.RealAirConditioner.SetTemperature(temp)
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

func (acp *AirConditionerProxy) TurnOn() {
	if !acp.authenticated {
		log.Println("Access denied: authentication required to turn on the air conditioner")
		return
	}
	acp.adapter.TurnOn()
}

func (acp *AirConditionerProxy) TurnOff() {
	if !acp.authenticated {
		log.Println("Access denied: authentication required to turn off the air conditioner")
		return
	}
	acp.adapter.TurnOff()
}

func (acp *AirConditionerProxy) SetTemperature(temp int) {
	if !acp.authenticated {
		log.Println("Access denied: authentication required to set the temperature on the air conditioner")
		return
	}
	acp.adapter.SetTemperature(temp)
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	return &AirConditionerProxy{
		adapter:       &AirConditionerAdapter{},
		authenticated: authenticated,
	}
}

func main() {
	airConditioner := NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}
