package easy

func subtractProductAndSum(n int) int {
	sum, proizv := 0, 1

	for n > 0 {
		sum += n % 10
		proizv *= n % 10
		n = n / 10
	}

	return proizv - sum
}
