package easy

func interpret(command string) string {
	mes := ""

	for i, v := range command {
		switch {
		case v == 'G':
			mes += string(v)
		case v == '(' && command[i+1] == ')':
			mes += "o"
		case v == '(' && command[i+1] == 'a':
			mes += "al"
		}
	}

	return mes
}
