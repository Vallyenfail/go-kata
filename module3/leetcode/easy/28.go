package easy

import (
	"strconv"
	"strings"
)

func countDigits(num int) int {
	counter := 0
	nums := strings.Split(strconv.Itoa(num), "")
	for _, v := range nums {
		val, _ := strconv.Atoi(v)
		if num%val == 0 {
			counter++
		}
	}

	return counter
}
