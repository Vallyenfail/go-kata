package easy

func numJewelsInStones(jewels string, stones string) int {

	counter := 0
	for _, v := range stones {
		for _, val := range jewels {
			if val == v {
				counter++
			}
		}
	}
	return counter
}
