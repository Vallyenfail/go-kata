package easy

func differenceOfSum(nums []int) int {
	sumElem := 0
	sumNums := 0
	absDiff := 0
	for _, num := range nums {
		sumElem += num
		if num > 9 && num < 100 {
			leftNum := num / 10
			rightNum := num % 10
			sumNums += leftNum + rightNum
		} else if num > 99 && num < 1000 {
			leftNum := num / 100
			midNum := (num - leftNum*100) / 10
			rightNum := num % 10
			sumNums += leftNum + midNum + rightNum
		} else if num > 999 {
			leftNum := num / 1000
			midNumleft := (num - leftNum*1000) / 100
			midNumright := (num - leftNum*1000 - midNumleft*100) / 10
			rightNum := num % 10
			sumNums += leftNum + midNumright + midNumleft + rightNum
		} else {
			sumNums += num
		}
	}
	absDiff = sumNums - sumElem
	if absDiff < 0 {
		absDiff = absDiff * -1
	}
	return absDiff
}
