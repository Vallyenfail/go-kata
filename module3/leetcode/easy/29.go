package easy

func xorOperation(n int, start int) int {
	num := 0
	i := start
	for n != 0 {
		num = num ^ i
		i += 2
		n--
	}

	return num
}
