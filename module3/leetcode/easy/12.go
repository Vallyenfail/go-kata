package easy

func numIdenticalPairs(nums []int) int {
	pairsCounter := 0

	for i := range nums {
		for j := range nums {
			if nums[i] == nums[j] && i < j {
				pairsCounter++
			}
		}
	}

	return pairsCounter
}
