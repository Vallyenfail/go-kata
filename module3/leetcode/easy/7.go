package easy

func defangIPaddr(address string) string {
	defAdress := ""

	for _, v := range address {
		if string(v) == "." {
			defAdress += "[.]"
			continue
		}
		defAdress += string(v)
	}

	return defAdress
}
