package easy

func runningSum(nums []int) []int {
	sum := make([]int, len(nums))

	for i := 0; i < len(nums); i++ {
		sums := 0
		for j := 0; j < i+1; j++ {
			sums += nums[j]
		}
		sum[i] = sums
	}
	return sum
}
