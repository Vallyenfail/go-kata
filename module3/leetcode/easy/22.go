package easy

func smallerNumbersThanCurrent(nums []int) []int {
	small := make([]int, len(nums))

	for i, v := range nums {
		counter := 0
		for j, val := range nums {
			if val != v && nums[j] < nums[i] {
				counter++
			}
		}
		small[i] = counter
	}
	return small
}
