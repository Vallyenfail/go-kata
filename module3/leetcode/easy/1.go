package easy

func tribonacci(n int) int {
	var triboSlice = []int{0, 1, 1}

	if n < 3 {
		return triboSlice[n]
	}

	for i := 3; i < n+1; i++ {
		tn := triboSlice[i-3] + triboSlice[i-2] + triboSlice[i-1]
		triboSlice = append(triboSlice, tn)
	}

	return triboSlice[len(triboSlice)-1]
}
