package easy

func decompressRLElist(nums []int) []int {
	output := make([]int, 0)
	for i := 0; i != len(nums); i = i + 2 {
		freq := nums[i]
		val := nums[i+1]
		for freq != 0 {
			output = append(output, val)
			freq--
		}
	}

	return output
}
