package easy

func createTargetArray(nums []int, index []int) []int {
	massive := make([]int, len(index))

	for i, v := range index {
		massive = append(massive[:v], append([]int{nums[i]}, massive[v:]...)...)
	}

	return massive
}
