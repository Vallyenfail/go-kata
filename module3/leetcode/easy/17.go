package easy

func mostWordsFound(sentences []string) int {
	maxNumWords := 0
	for _, sentence := range sentences {
		counter := 0
		for _, letter := range sentence {
			if string(letter) == " " {
				counter++
			}
		}
		if maxNumWords < counter {
			maxNumWords = counter
		}
	}
	return maxNumWords
}
