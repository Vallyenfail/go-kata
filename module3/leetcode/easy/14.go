package easy

func maximumWealth(accounts [][]int) int {
	mostRich := 0

	for _, w := range accounts {
		rich := 0
		for _, r := range w {
			rich += r
		}
		if rich > mostRich {
			mostRich = rich
		}
	}
	return mostRich
}
