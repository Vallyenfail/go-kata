package easy

func shuffle(nums []int, n int) []int {
	leftNum := nums[:n]
	rightNum := nums[n:]
	wholeNum := make([]int, len(nums))

	for i, j := 0, 0; i < len(leftNum); i, j = i+1, j+1 {
		wholeNum[i*2] = leftNum[j]
		wholeNum[i*2+1] = rightNum[j]
	}
	return wholeNum
}
