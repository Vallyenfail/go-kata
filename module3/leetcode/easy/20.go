package easy

func kidsWithCandies(candies []int, extraCandies int) []bool {
	candeez := make([]bool, len(candies))
	max := 0

	for _, candy := range candies {
		if candy > max {
			max = candy
		}
	}

	for i, candy := range candies {
		if candy+extraCandies >= max {
			candeez[i] = true
		}
	}
	return candeez
}
