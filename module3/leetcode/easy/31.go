package easy

func sortPeople(names []string, heights []int) []string {
	for i := 0; i < len(names)-1; i++ {
		for j := i + 1; j < len(names); j++ {
			if heights[j] > heights[i] {
				heights[i], heights[j] = heights[j], heights[i]
				names[i], names[j] = names[j], names[i]
			}
		}
	}
	return names
}
