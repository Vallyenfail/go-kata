package easy

func finalValueAfterOperations(operations []string) int {
	x := 0
	for _, v := range operations {
		switch {
		case v == "X++" || v == "++X":
			x++
		case v == "X--" || v == "--X":
			x--
		}
	}
	return x
}
