package medium

func sortTheStudents(score [][]int, k int) [][]int {
	swapped := true
	j := 1
	for swapped {
		swapped = false
		for i := 0; i < len(score)-j; i++ {
			if score[i][k] < score[i+1][k] {
				score[i], score[i+1] = score[i+1], score[i]
				swapped = true
			}
		}
		j++
	}
	return score
}
