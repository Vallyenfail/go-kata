package medium

func pairSum(head *ListNode) int {
	sum := 0
	twin := twins(head)

	for i := 0; i < len(twin)/2; i++ {
		sumTwins := twin[len(twin)-1-i] + head.Val
		if sum < sumTwins {
			sum = sumTwins
		}
		head = head.Next
	}
	return sum
}

func twins(head *ListNode) []int {
	var twins []int
	for head != nil {
		twins = append(twins, head.Val)
		head = head.Next
	}
	return twins
}

//Interesting solution

//func pairSum(head *ListNode) int {
//	mid := findMid(head)
//	reverseMid := reverseList(mid)
//
//	return findMax(head, reverseMid)
//}
//
//func findMid(head *ListNode) *ListNode {
//	slow, fast := head, head
//	for fast != nil && fast.Next != nil {
//		slow = slow.Next
//		fast = fast.Next.Next
//	}
//	return slow
//}
//
//func reverseList(head *ListNode) *ListNode {
//	var prev *ListNode
//	cur := head
//
//	for cur != nil {
//		next := cur.Next
//		cur.Next = prev
//		prev = cur
//		cur = next
//	}
//
//	return prev
//}
//
//func findMax(l1 *ListNode, l2 *ListNode) int {
//	max := math.MinInt
//	for l1 != nil && l2 != nil {
//		max = countMax(max, l1.Val+l2.Val)
//		l1 = l1.Next
//		l2 = l2.Next
//	}
//	return max
//}
//
//func countMax(a int, b int) int {
//	if a > b {
//		return a
//	}
//	return b
//}
