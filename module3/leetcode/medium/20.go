package medium

func processQueries(queries []int, m int) []int {
	var arr []int
	var p []int
	idx := 1

	for idx != m+1 {
		p = append(p, idx)
		idx++
	}

	for _, num := range queries {
		j := 0
		for num != p[j] {
			j++
		}
		arr = append(arr, j)
		p = append([]int{num}, p...)
		p = append(p[:j+1], p[j+2:]...)
	}
	return arr
}
