package medium

import "sort"

func numTilePossibilities(tiles string) int {
	bytes := []byte(tiles)
	sort.Slice(bytes, func(i, j int) bool {
		return bytes[i] < bytes[j]
	})
	tiles = string(bytes)

	inUse := make([]bool, len(tiles))
	count := 0

	var dfs func()
	dfs = func() {
		count++
		for i := 0; i < len(tiles); i++ {
			if inUse[i] {
				continue
			}
			if i > 0 && tiles[i-1] == tiles[i] && !inUse[i-1] {
				continue
			}
			inUse[i] = true
			dfs()
			inUse[i] = false
		}
	}
	dfs()

	return count - 1 //-1 because of empty set
}
