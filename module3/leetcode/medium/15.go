package medium

func minPartitions(n string) int {
	best := '0'
	for _, num := range n {
		if num > best {
			best = num
		}
		if best == '9' {
			return int(best - '0')
		}
	}
	return int(best - '0')
}
