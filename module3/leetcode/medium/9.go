package medium

import (
	"math"
	"sort"
)

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	var arr = []bool{}

	for i := range l {
		num := make([]int, 0)
		num = append(num, nums[l[i]:r[i]+1]...)
		arith := isArithmetic(num)
		arr = append(arr, arith)
	}
	return arr
}

func isArithmetic(nums []int) bool {
	if len(nums) == 2 {
		return true
	}

	sort.Slice(nums, func(i, j int) bool {
		return int(math.Abs(float64(nums[i]))) < int(math.Abs(float64(nums[j])))
	})

	for i := 0; i < len(nums)-1; i++ {
		if nums[i+1]-nums[i] != nums[1]-nums[0] {
			return false
		}
	}
	return true
}
