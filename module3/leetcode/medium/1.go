package medium

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deepestLeavesSum(root *TreeNode) int {
	d := height(root)
	i := 1

	return sum(root, i, d)
}

func height(root *TreeNode) int {
	if root != nil {
		x := height(root.Left)
		y := height(root.Right)

		if x > y {
			return x + 1
		} else {
			return y + 1
		}
	}
	return 0
}

func sum(root *TreeNode, i, d int) int {
	if root != nil {
		if i == d {
			return root.Val
		}
		x := sum(root.Left, i+1, d)
		y := sum(root.Right, i+1, d)

		return x + y
	}
	return 0
}
