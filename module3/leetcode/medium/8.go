package medium

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	inputs := make([]int, n)
	output := make([]int, 0)
	for _, edge := range edges {
		inputs[edge[1]]++
	}
	for i, in := range inputs {
		if in == 0 {
			output = append(output, i)
		}
	}
	return output
}
