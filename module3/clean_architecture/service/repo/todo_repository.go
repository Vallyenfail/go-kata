package repo

import (
	"errors"

	"gitlab.com/Vallyenfail/go-kata/module3/clean_architecture/service/model"
)

// TaskRepository is a repository interface for tasks

type TodoRepository interface {
	ListTodos() []*model.Todo
	CreateTodo(todo *model.Todo) error
	CompleteTodo(id int) error
	RemoveTodo(id int) error
}

type TodoLocalStorage struct {
	Todos map[int]*model.Todo
}

func NewTodoLocalStorage() *TodoLocalStorage {
	return &TodoLocalStorage{Todos: make(map[int]*model.Todo)}
}

func (r *TodoLocalStorage) ListTodos() []*model.Todo {
	todos := make([]*model.Todo, 0)

	if len(r.Todos) == 0 {
		return todos
	}

	for _, todo := range r.Todos {
		todos = append(todos, todo)
	}

	return todos
}

func (r *TodoLocalStorage) CreateTodo(todo *model.Todo) error {
	r.Todos[todo.ID] = todo

	return nil
}

func (r *TodoLocalStorage) CompleteTodo(id int) error {
	err := r.RemoveTodo(id)
	if err != nil {
		return err
	}

	return nil
}

func (r *TodoLocalStorage) RemoveTodo(id int) error {
	for k := range r.Todos {
		if k == id {
			delete(r.Todos, k)
			return nil
		}
	}

	return errors.New("todo is not found")
}
