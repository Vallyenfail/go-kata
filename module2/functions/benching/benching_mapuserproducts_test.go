package benching

import "testing"

func BenchmarkMapUserProducts2(b *testing.B) {
	user := genUsers()
	products := genProducts()
	MapUserProducts2(user, products)
}

func BenchmarkMapUserProducts(b *testing.B) {
	user := genUsers()
	products := genProducts()
	MapUserProducts(user, products)
}
