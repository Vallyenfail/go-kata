package main

import (
	"fmt"
	"testing"
	"unicode"
)

func TestGreet(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "english name",
			args: args{name: "John"},
			want: "Hello, John, you are welcome!",
		},
		{
			name: "russian name",
			args: args{name: "Боря"},
			want: "Привет, Боря, добро пожаловать!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Greet(tt.args.name); got != tt.want {
				t.Errorf("Greet() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Greet(name string) string {
	if len(name) != 0 {
		for _, symbol := range name {
			if unicode.Is(unicode.Cyrillic, symbol) {
				return fmt.Sprintf("Привет, %s, добро пожаловать!", name)
			}
		}
		for _, symbol := range name {
			if unicode.Is(unicode.Latin, symbol) {
				return fmt.Sprintf("Hello, %s, you are welcome!", name)
			}
		}
	}
	return ""
}
