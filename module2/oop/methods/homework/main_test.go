package main

import "testing"

func TestMultiply(t *testing.T) {
	tests := []struct {
		name string
		a    float64
		b    float64
		want float64
	}{
		{
			name: "1",
			a:    2,
			b:    2,
			want: 4,
		}, {
			name: "2",
			a:    5.1,
			b:    6.2,
			want: 31.619999999999997,
		}, {
			name: "3",
			a:    0,
			b:    0,
			want: 0,
		}, {
			name: "4",
			a:    1,
			b:    1,
			want: 1,
		}, {
			name: "5",
			a:    6,
			b:    8,
			want: 48,
		}, {
			name: "6",
			a:    5,
			b:    5,
			want: 25,
		}, {
			name: "7",
			a:    0.1,
			b:    0.1,
			want: 0.010000000000000002,
		}, {
			name: "8",
			a:    999999,
			b:    888888,
			want: 888887111112,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := multiply(tt.a, tt.b); got != tt.want {
				t.Errorf("multiply() = %v, want = %v", got, tt.want)
			}
		})
	}
}

func TestDivide(t *testing.T) {
	tests := []struct {
		name string
		a    float64
		b    float64
		want float64
	}{
		{
			name: "1",
			a:    2,
			b:    2,
			want: 1,
		}, {
			name: "2",
			a:    6.2,
			b:    0.2,
			want: 31.0,
		}, {
			name: "3",
			a:    0,
			b:    4,
			want: 0,
		}, {
			name: "4",
			a:    1,
			b:    1,
			want: 1,
		}, {
			name: "5",
			a:    6,
			b:    8,
			want: 0.75,
		}, {
			name: "6",
			a:    625,
			b:    5,
			want: 125,
		}, {
			name: "7",
			a:    9.9,
			b:    3.3,
			want: 3.0000000000000004,
		}, {
			name: "8",
			a:    999999,
			b:    888888,
			want: 1.125,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := divide(tt.a, tt.b); got != tt.want {
				t.Errorf("divide() = %v, want = %v", got, tt.want)
			}
		})
	}
}

func TestSum(t *testing.T) {
	tests := []struct {
		name string
		a    float64
		b    float64
		want float64
	}{
		{
			name: "1",
			a:    2,
			b:    2,
			want: 4,
		}, {
			name: "2",
			a:    5.1,
			b:    6.2,
			want: 11.3,
		}, {
			name: "3",
			a:    0,
			b:    0,
			want: 0,
		}, {
			name: "4",
			a:    1,
			b:    1,
			want: 2,
		}, {
			name: "5",
			a:    6,
			b:    8,
			want: 14,
		}, {
			name: "6",
			a:    5,
			b:    5,
			want: 10,
		}, {
			name: "7",
			a:    0.1,
			b:    0.1,
			want: 0.2,
		}, {
			name: "8",
			a:    999999,
			b:    888888,
			want: 1888887,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sum(tt.a, tt.b); got != tt.want {
				t.Errorf("sum() = %v, want = %v", got, tt.want)
			}
		})
	}
}

func TestAverage(t *testing.T) {
	tests := []struct {
		name string
		a    float64
		b    float64
		want float64
	}{
		{
			name: "1",
			a:    2,
			b:    2,
			want: 2,
		}, {
			name: "2",
			a:    5.1,
			b:    6.2,
			want: 5.65,
		}, {
			name: "3",
			a:    0,
			b:    0,
			want: 0,
		}, {
			name: "4",
			a:    1,
			b:    1,
			want: 1,
		}, {
			name: "5",
			a:    6,
			b:    8,
			want: 7,
		}, {
			name: "6",
			a:    10,
			b:    5,
			want: 7.5,
		}, {
			name: "7",
			a:    0.1,
			b:    0.1,
			want: 0.1,
		}, {
			name: "8",
			a:    999999,
			b:    888888,
			want: 944443.5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := average(tt.a, tt.b); got != tt.want {
				t.Errorf("average() = %v, want = %v", got, tt.want)
			}
		})
	}
}
