package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}

type Wallet struct {
	RUB uint64
	USD uint64
	BTC uint64
	ETH uint64
}

type Location struct {
	Address string
	City    string
	index   string
}

func main() {
	user := User{
		Age:  13,
		Name: "Sanya",
	}
	fmt.Println(user)
	wallet := Wallet{
		RUB: 250000,
		USD: 3500,
		BTC: 1,
		ETH: 4,
	}
	user.Wallet = wallet
	fmt.Println(user)

	user2 := User{
		Age:  34,
		Name: "Vovka",
		Wallet: Wallet{
			RUB: 199900,
			USD: 800,
			BTC: 8,
			ETH: 4,
		},
		Location: Location{
			Address: "India, ishabat 3, 768",
			City:    "Dehli",
			index:   "108836",
		},
	}
	fmt.Println(user2)

	fmt.Println(wallet)
	fmt.Println("wallet allocates:", unsafe.Sizeof(wallet), "bytes")
}
