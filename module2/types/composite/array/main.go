package main

import "fmt"

func main() {
	testArray()
	rangeArray()
}

type User struct {
	Age    int
	Name   string
	Wallet Wallet
}

type Wallet struct {
	RUB uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func testArray() {
	a := [...]int{34, 55, 89, 144}

	fmt.Println("original value", a)

	a[0] = 21

	fmt.Println("changed first element", a)

	b := a

	a[0] = 233
	fmt.Println("original array", a)
	fmt.Println("modified array", b)
}

func rangeArray() {
	users := [4]User{
		{
			Age:  8,
			Name: "John",
			Wallet: Wallet{
				RUB: 180,
				USD: 20,
				BTC: 2,
				ETH: 1,
			},
		},
		{
			Age:  13,
			Name: "Katie",
			Wallet: Wallet{
				RUB: 500,
				USD: 3,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			Age:  21,
			Name: "Doe",
			Wallet: Wallet{
				RUB: 0,
				USD: 300,
				BTC: 1,
				ETH: 3,
			},
		},
		{
			Age:  34,
			Name: "Arnie",
			Wallet: Wallet{
				RUB: 987352,
				USD: 34,
				BTC: 1,
				ETH: 3,
			},
		},
	}

	fmt.Println("Users above 18 age:")
	for i := range users {
		if users[i].Age > 18 {
			fmt.Println(users[i])
		}
	}
	fmt.Println("Users who has crypto in balance:")
	for i, user := range users {
		if users[i].Wallet.BTC > 0 || users[i].Wallet.ETH > 0 {
			fmt.Println("user:", user.Name, "index:", i)
		}
	}
	fmt.Println("=== end users non zero crypto balance ===")
}
