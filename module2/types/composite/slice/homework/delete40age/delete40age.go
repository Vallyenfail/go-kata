package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
		{
			Name: "Rose",
			Age:  41,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	for s := 0; s < len(users); s++ {
		if users[s].Age > 40 {
			users = append(users[:s], users[s+1:]...)
			s--
		}
	}
	fmt.Println(users)
}
