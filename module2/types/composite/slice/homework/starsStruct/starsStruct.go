// You can edit this code!
// Click here and start typing.
package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 28500,
		},
		{
			Name:  "https://github.com/AutumnWhj/ChatGPT-wechat-bot",
			Stars: 2200,
		},
		{
			Name:  "https://github.com/Qiskit/qiskit-terra",
			Stars: 3500,
		},
		{
			Name:  "https://github.com/kubeflow/kubeflow",
			Stars: 12200,
		},
		{
			Name:  "https://github.com/kizniche/Mycodo",
			Stars: 2400,
		},
		{
			Name:  "https://github.com/apache/spark",
			Stars: 35000,
		},
		{
			Name:  "https://github.com/ansible/ansible",
			Stars: 56200,
		},
		{
			Name:  "https://github.com/tensorflow/tensorflow",
			Stars: 171000,
		},
		{
			Name:  "https://github.com/apple/swift",
			Stars: 61800,
		},
		{
			Name:  "https://github.com/golang/go",
			Stars: 108000,
		},
		{
			Name:  "https://github.com/pandas-dev/pandas",
			Stars: 36800,
		},
		{
			Name:  "https://github.com/keybase/client",
			Stars: 8400,
		},
		{
			Name:  "https://github.com/CleverRaven/Cataclysm-DDA",
			Stars: 8100,
		},
	}

	// в цикле запишите в map
	projectsMap := map[string]Project{}
	for k := range projects {
		projectsMap[projects[k].Name] = projects[k]
	}
	// в цикле пройдитесь по мапе и выведите значения в консоль
	for _, v := range projectsMap {
		fmt.Println(v)
	}
}
