package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	s = AppendSecond(s, 4)
	fmt.Println(s)
}

func AppendSecond(s []int, elem int) []int {
	capS := len(s)
	if capS == cap(s) {
		newSlice := make([]int, len(s), 2*cap(s))
		copy(newSlice, s)
		s = newSlice
	}
	s = s[0 : capS+1]
	s[capS] = elem
	return s
}
