package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	s = AppendFirst(s)
	fmt.Println(s)
}

func AppendFirst(s []int) []int {
	s = append(s, 4)
	return s
}
