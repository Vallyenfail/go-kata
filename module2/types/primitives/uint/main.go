package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeUint()
}

func typeUint() {
	fmt.Println("=== START type uint ===")
	var numberUnt8 uint8 = 1 << 1
	fmt.Println("left shift uint8:", numberUnt8, "size:", unsafe.Sizeof(numberUnt8), "bytes")
	var number2Unt8 uint8 = 17 >> 1
	fmt.Println("right shift uint8:", number2Unt8, "size:", unsafe.Sizeof(number2Unt8), "bytes")
	numberUnt8 = (1 << 8) - 1
	fmt.Println("uint8 max value:", numberUnt8, "size:", unsafe.Sizeof(numberUnt8), "bytes")
	var numberUnt16 uint16 = (1 << 16) - 1
	fmt.Println("uint16 max value:", numberUnt16, "size:", unsafe.Sizeof(numberUnt16), "bytes")
	var numberUnt32 uint32 = (1 << 32) - 1
	fmt.Println("uint32 max value:", numberUnt32, "size:", unsafe.Sizeof(numberUnt32), "bytes")
	var numberUnt64 uint64 = (1 << 64) - 1
	fmt.Println("uint64 max value:", numberUnt64, "size:", unsafe.Sizeof(numberUnt64), "bytes")
	fmt.Println("=== END type uint ===")
}
