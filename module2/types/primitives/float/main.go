package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeFloat()
}

func typeFloat() {
	fmt.Println("=== START type float ===")
	// calculate the exponent by the scheme
	var uintNumber uint32 = 1 << 29 // making shift onto 29 cells into 30 position
	uintNumber += 1 << 28           // adding 1 to 29 position and so on
	uintNumber += 1 << 27
	uintNumber += 1 << 26
	uintNumber += 1 << 25
	// add mantis
	uintNumber += 1 << 21 // one to 22 position
	var floatNumber = *(*float32)(unsafe.Pointer(&uintNumber))
	fmt.Println(floatNumber)

	a, b := 2.3329, 3.1234
	c := a + b
	fmt.Println("mistake 1:", c) // 5.456300000001

	a = 9.99999
	b2 := float64(a)
	fmt.Println("mistake 2:", b2)

	a = 999998455
	b3 := float32(a)
	fmt.Println("mistake 3:", "%f\n", b3)

	c4 := 5.2
	d4 := 2.1

	fmt.Println(c4 + d4)
	fmt.Println((c4 + d4) == 7.3) // will return FALSE

	fmt.Println("=== END type float ===")
}
