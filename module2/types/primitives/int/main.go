package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeInt()
}

func typeInt() {
	fmt.Println("=== Start type int ===")
	var intNumber8 uint8 = 1 << 7
	var min8 = int8(intNumber8)
	intNumber8--
	var max8 = int8(intNumber8)
	fmt.Println("int8 min value:", min8, "int8 max value:", max8, "size:", unsafe.Sizeof(min8), "bytes")
	var intNumber16 uint16 = 1 << 15
	var min16 = int16(intNumber16)
	intNumber16--
	var max16 = int16(intNumber16)
	fmt.Println("int16 min value:", min16, "int16 max value:", max16, "size:", unsafe.Sizeof(min16), "bytes")
	var intNumber32 uint32 = 1 << 31
	var min32 = int32(intNumber32)
	intNumber32--
	var max32 = int32(intNumber32)
	fmt.Println("int32 min value:", min32, "int32 max value:", max32, "size:", unsafe.Sizeof(min32), "bytes")
	var intNumber64 uint64 = 1 << 63
	var min64 = int64(intNumber64)
	intNumber64--
	var max64 = int64(intNumber64)
	fmt.Println("int64 min value:", min64, "int64 max value:", max64, "size:", unsafe.Sizeof(min64), "bytes")
	fmt.Println("=== END type int ===")
}
