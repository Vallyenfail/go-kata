package main

import "fmt"

func main() {
	var numberInt = 3
	var numberFloat = float32(numberInt)

	fmt.Printf("type: %T, value: %v", numberFloat, numberFloat)
}
