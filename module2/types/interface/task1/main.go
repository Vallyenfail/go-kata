// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	var x MyInterface
	switch v := x.(type) {
	case nil:
		test(v)
	case *int:
		fmt.Println(n == nil) // won't work
		test(n)
	}
}

func test(r interface{}) {
	if r == nil {
		fmt.Println("Success!")
	}
}
