package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

func main() {
	t := time.Now()
	rand.Seed(t.UnixNano())

	go parseURL("http://example.com/")
	parseURL("http://youtube.com/")

	fmt.Printf("Parsing completed. Time Elapsed: %f seconds\n", time.Since(t).Seconds())

	fmt.Println("__________________________________________________________________________")

	urls := []string{
		"https://google.com/",
		"https://youtube.com/",
		"https://github.com/",
		"https://medium.com/",
	}

	var wg sync.WaitGroup

	for _, url := range urls {
		wg.Add(1)

		go func(url string) {
			doHTTP(url)
			wg.Done()
		}(url)
	}

	wg.Wait()

	fmt.Println("__________________________________________________________________________")

	//message1 := make(chan string)
	//message2 := make(chan string)
	//
	//go func() {
	//	for {
	//		time.Sleep(time.Millisecond * 500)
	//		message1 <- "half a second"
	//	}
	//}()
	//
	//go func() {
	//	for {
	//		time.Sleep(time.Second * 2)
	//		message2 <- "2 seconds"
	//	}
	//}()
	//
	//for {
	//	select {
	//	case msg := <-message1:
	//		fmt.Println(msg)
	//	case msg := <-message2:
	//		fmt.Println(msg)
	//	}
	//}
}

func parseURL(url string) {
	for i := 0; i < 5; i++ {
		latency := rand.Intn(500) + 500

		time.Sleep(time.Duration(latency) * time.Millisecond)

		fmt.Printf("Parsing <%s> - Step %d - Latency %d\n", url, i+1, latency)
	}
}

func doHTTP(url string) {
	t := time.Now()

	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("Failed to get <%s>: %s\n", url, err.Error())
	}

	defer resp.Body.Close()

	fmt.Printf("<%s> - Status Code [%d] - Latency %d ms\n", url, resp.StatusCode, time.Since(t).Milliseconds())
}
