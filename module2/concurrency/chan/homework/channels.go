package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int, 2)
	b := make(chan int, 2)
	c := make(chan int, 2)

	out := generateData()

	timerA := time.NewTimer(30 * time.Second)
	timerB := time.NewTimer(30 * time.Second)
	timerC := time.NewTimer(30 * time.Second)

	go func() {
		for num := range out {
			select {
			case a <- num:
			case t := <-timerA.C:
				close(a)
				fmt.Println("Closed a at", t)
				return
			}
		}
	}()

	go func() {
		for num := range out {
			select {
			case b <- num:
			case t := <-timerB.C:
				close(b)
				fmt.Println("Closed b at", t)
				return
			}
		}
	}()

	go func() {
		for num := range out {
			select {
			case c <- num:
			case t := <-timerC.C:
				close(c)
				fmt.Println("Closed c at", t)
				return
			}
		}
	}()

	mainChan := joinChannels(a, b, c)
	for num := range mainChan { //Channel is closed automatically
		fmt.Println(num)
	}
}
