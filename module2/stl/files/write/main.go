package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"unicode"
)

func createFile(content string) {
	filePtr, err := os.Create("./example.txt")
	if err != nil {
		log.Fatal(err)
	}
	_, err = filePtr.WriteString(content)
	if err != nil {
		log.Fatal(err)
	}
	filePtr.Close()
}

func readFileContents() string {
	bytes, err := os.ReadFile("example.txt")
	if err != nil {
		log.Fatal(err)
	}
	fileText := string(bytes[:])
	return fileText
}

var toTranslit = map[string]string{
	"а": "a",
	"б": "b",
	"в": "v",
	"г": "g",
	"д": "d",
	"е": "e",
	"ё": "yo",
	"ж": "zh",
	"з": "z",
	"и": "i",
	"й": "j",
	"к": "k",
	"л": "l",
	"м": "m",
	"н": "n",
	"о": "o",
	"п": "p",
	"р": "r",
	"с": "s",
	"т": "t",
	"у": "u",
	"ф": "f",
	"х": "h",
	"ц": "c",
	"ч": "ch",
	"ш": "sh",
	"щ": "sch",
	"ъ": "'",
	"ы": "y",
	"ь": "",
	"э": "e",
	"ю": "ju",
	"я": "ja",
}

func translitIt(s string, table map[string]string) string {
	out := ""
	for _, runa := range s {
		if string(runa) == " " {
			out += " "
			continue
		}
		out += table[string(unicode.ToLower(runa))]
	}
	return out
}

func main() {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your text: ")

	text, _ := reader.ReadString('\n')
	createFile(text)

	fileText := translitIt(readFileContents(), toTranslit)

	//content := "съешь ещё этих мягких французских булок да выпей чаю"

	err := os.WriteFile("./example.processed.txt", []byte(fileText), 0666)
	if err != nil {
		log.Fatal(err)
	}
}
