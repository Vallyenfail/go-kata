package main

import (
	"encoding/json"
	"fmt"
)

// Student declares `Student` map
type Student map[string]interface{}

func main() {

	// some JSON data
	data := []byte(`
	{
		"id": 123,
		"fname": "John",
		"height": 1.75,
		"male": true,
		"languages": null,
		"subjects": [ "Math", "Science" ],
		"profile": {
			"uname": "johndoe91",
			"f_count": 1975
		}
	}`)

	// create a data container
	var john Student

	// unmarshal `data`
	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))

	// print `john` map
	fmt.Printf("%#v\n\n", john)

	// iterate through keys and values
	i := 1
	for k, v := range john {
		fmt.Printf("%d: key (`%T`)`%v`, value (`%T`)`%#v`\n", i, k, k, v, v)
		i++
	}
}
