package main

import (
	"bytes"
	"encoding/json"
	"fmt"
)

type Person struct {
	Name string
	Age  int
}

func main() {

	// create a buffer to hold JSON data
	buf := new(bytes.Buffer)
	// create JSON encoder for `buf`
	bufEncoder := json.NewEncoder(buf)

	// encode JSON from `Person` structs
	err := bufEncoder.Encode(Person{"Ross Geller", 28})
	if err != nil {
		return
	}
	err = bufEncoder.Encode(Person{"Monica Geller", 27})
	if err != nil {
		return
	}
	err = bufEncoder.Encode(Person{"Jack Geller", 56})
	if err != nil {
		return
	}

	// print contents of the `buf`
	fmt.Println(buf) // calls `buf.String()` method

}
