package main

import (
	"encoding/json"
	"fmt"
)

// Profile declares `Profile` structure
type Profile struct {
	Username  string
	Followers int
}

// Account declares `Account` structure
type Account struct {
	IsMale bool
	Email  string
}

// Student declares `Student` structure
type Student struct {
	FirstName, LastName string
	HeightInMeters      float64
	IsMale              bool
	Profile
	Account
}

func main() {

	// some JSON data
	data := []byte(`
	{
		"FirstName": "John",
		"HeightInMeters": 1.75,
		"IsMale": true,
		"Username": "johndoe91",
		"Followers": 1975,
		"Account": { "IsMale": true, "Email": "john@doe.com" }
	}`)

	// create a data container
	var john Student

	// unmarshal `data`
	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))

	// print `john` struct
	fmt.Printf("%#v\n", john)
}
