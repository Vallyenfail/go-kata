package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Person struct {
	Name string
	Age  int
}

func main() {

	// create a strings reader
	jsonStream := strings.NewReader(`
{"Name":"Ross Geller","Age":28}
{"Name":"Monica Geller","Age":27}
{"Name":"Jack Geller","Age":56}
`)

	// create JSON decoder using `jsonStream`
	decoder := json.NewDecoder(jsonStream)

	// create `Person` structs to hold decoded data
	var ross, monica Person

	// decode JSON from `decoder` one line at a time
	err := decoder.Decode(&ross)
	if err != nil {
		return
	}
	err = decoder.Decode(&monica)
	if err != nil {
		return
	}

	// see value of the `ross` and `monica`
	fmt.Printf("ross: %#v\n", ross)
	fmt.Printf("monica: %#v\n", monica)
}
