package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	Followers int
}

type Student struct {
	FirstName, LastName string
	HeightInMeters      float64
	IsMale              bool
	Languages           [2]string
	Subjects            []string
	Grades              map[string]string
	Profile             *Profile
}

func main() {

	// some JSON data
	data := []byte(`
	{
		"FirstName": "John",
		"HeightInMeters": 1.75,
		"IsMale": null,
		"Languages": [ "English" ],
		"Subjects": [ "Math", "Science" ],
		"Grades": null,
		"Profile": { "Followers": 1975 }
	}`)

	// create a data container
	var john Student = Student{
		IsMale:    true,
		Languages: [2]string{"Korean", "Chinese"},
		Subjects:  nil,
		Grades:    map[string]string{"Math": "A"},
		Profile:   &Profile{Username: "johndoe91"},
	}

	// unmarshal `data`
	fmt.Printf("Error: %v\n\n", json.Unmarshal(data, &john))

	fmt.Printf("%#v\n\n", john)
	fmt.Printf("%#v\n", john.Profile)
}
