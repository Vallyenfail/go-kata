package main

import (
	"encoding/json"
	"fmt"
)

type Student struct {
	FirstName, LastName string
	Email               string
	Age                 int
	HeightInMeters      float64
}

func main() {

	// some JSON data
	data := []byte(`
	{
		"FirstName": "John",
		"LastName": "Doe",
		"Age": 21,
		"HeightInMeters": 175,
		"Username": "johndoe91"
	}`)

	// create a data container
	var john Student

	// unmarshal `data`
	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))

	// print `john` struct
	fmt.Printf("%#v\n", john)
}
