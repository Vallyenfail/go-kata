package main

import (
	"encoding/json"
	"fmt"
)

// Profile declares `Profile` structure
type Profile struct {
	Username  string `json:"uname"`
	Followers int    `json:"f_count"`
}

// Student declares `Student` structure
type Student struct {
	FirstName      string   `json:"fname"`
	LastName       string   `json:"-"` // discard
	HeightInMeters float64  `json:"height"`
	IsMale         bool     `json:"male"`
	Languages      []string `json:",omitempty"`
	Profile        Profile  `json:"profile"`
}

func main() {

	// some JSON data
	data := []byte(`
	{
		"fname": "John",
		"LastName": "Doe",
		"height": 1.75,
		"IsMale": true,
		"Languages": null,
		"profile": {
			"uname": "johndoe91",
			"Followers": 1975
		}
	}`)

	// create a data container
	var john Student = Student{
		Languages: []string{"English", "French"},
	}

	// unmarshal `data`
	fmt.Printf("Error: %v\n", json.Unmarshal(data, &john))

	// print `john` struct
	fmt.Printf("%#v\n", john)
}
