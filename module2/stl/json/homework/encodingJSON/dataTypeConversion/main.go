package main

import (
	"encoding/json"
	"fmt"
)

// Profile declares `Profile` structure
type Profile struct {
	Username  string
	Followers int
}

// MarshalJSON - implement `Marshaler` interface
func (p Profile) MarshalJSON() ([]byte, error) {
	// return JSON value (TODO: handle error gracefully)
	return []byte(fmt.Sprintf(`{"f_count": "%d"}`, p.Followers)), nil
}

// Age declares `Age` type
type Age int

// MarshalText - implement `TextMarshaler` interface
func (a Age) MarshalText() ([]byte, error) {
	// return string value (TODO: handle error gracefully)
	return []byte(fmt.Sprintf(`{"age": %d}`, int(a))), nil
}

// Student declares `Student` structure
type Student struct {
	FirstName, lastName string
	Age                 Age
	Profile             Profile
}

func main() {

	// define `john` struct (pointer)
	john := &Student{
		FirstName: "John",
		lastName:  "Doe",
		Age:       21,
		Profile: Profile{
			Username:  "johndoe91",
			Followers: 1975,
		},
	}

	// encode `john` as JSON
	johnJSON, _ := json.MarshalIndent(john, "", "  ")

	// print JSON string
	fmt.Println(string(johnJSON))
}
