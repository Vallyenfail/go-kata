package main

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	database, _ := sql.Open("sqlite3", "./gopher.db")

	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	_, err := statement.Exec()
	if err != nil {
		return
	}

	statement, _ = database.Prepare("INSERT INTO people (firstname, lastname) VALUES (?, ?)")
	_, err = statement.Exec("Lor", "Ipum")
	if err != nil {
		return
	}

	rows, _ := database.Query("SELECT id, firstname FROM people")
	var id int
	var firstname string

	for rows.Next() {
		err := rows.Scan(&id, &firstname)
		if err != nil {
			return
		}
		fmt.Printf("%d: %s \n", id, firstname)
	}
}
