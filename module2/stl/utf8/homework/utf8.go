package main

import (
	"fmt"
	"regexp"
	"strings"
	"unicode"
)

var data = ` Сообщения с телеграм канала Go-go!
я, извините, решил, что вы хоть что-то про регулярки знаете 🙂
С Почтением, с уважением 🙂.

Сообщения с телеграм канала Go-get job:
#Вакансия #Vacancy #Удаленно #Remote #Golang #Backend  
👨‍💻  Должность: Golang Tech Lead
🏢  Компания: PointPay
💵  Вилка: от 9000 до 15000 $ до вычета налогов
🌏  Локация: Великобритания
💼  Формат работы: удаленный
💼  Занятость: полная занятость 

Наша компания развивает 15 продуктов в сфере блокчейн и криптовалюты, функционирующих в рамках единой экосистемы. Мы - одни из лучших в своей нише, и за без малого три года мы создали криптовалютную платформу, которая предоставляет комплексные решения и позволяет пользователям проводить практически любые финансовые операции.
Наш проект победил в номинации "лучший блокчейн-стартап 2019 года" на одной из самых крупных конференций по блокчейн, криптовалютам и майнингу – Blockchain Life в Москве.

У нас очень амбициозные планы по развитию наших продуктов, и на данный момент мы находимся в поиске Golang Technical Lead, чтобы вместе создать революционный и самый технологичный продукт в мире.


  
Мы ожидаем от тебя:  
  
✅ Опыт управления финансовыми IT продуктами;
✅ Опыт постановки задач технической команде и приема результатов;
✅ Понимание и использование современных подходов к разработке (Agile);
✅ Опыт разработки не менее 5 лет;
✅ Отличное владение Go, будет плюсом опыт работы на PHP;
✅ Опыт работы с REST / GRPС (protobuf);
✅ Опыт работы с Laravel;
✅ Отличные навыки SQL (мы используем PostgreSQL);
✅ Опыт работы с Redis или аналогичной системой кеширования;
✅ Опыт работы с Kubernetes, с брокерами сообщений (RabbitMQ, Kafka и др.);
✅ Базовые знания Amazon Web Services.

Будем плюсом:

✅ Опыт разработки криптовалютных кошельков / криптовалютных бирж / криптовалютных торговых роботов / криптовалютных платежных систем или опыт работы в FinTech или банковских продуктах.

Чем предстоит заниматься:  
  
📌 Управлением и взаимодействием с командой;
📌 Постановкой задач команде разработчиков;
📌 Разработкой решений для запуска и сопровождения разрабатываемого компанией ПО;
📌 Выстраиванием эффективного процесса разработки;
📌 Достигать результатов, поддерживать и развивать текущие проекты, развивать новые проекты и продукты, искать и предлагать нестандартные решения задач.

Мы гарантируем будущему коллеге:  
  
🔥 Высокую заработную плату до 15 000$ (в валюте);
🔥 Индексацию заработной платы;
🔥 Полностью удаленный формат работы и максимально гибкий график - работайте из любой точки мира в удобное вам время;
🔥 Оформление по бессрочному международному трудовому договору с UK;
🔥 Отсутствие бюрократии и кучи бессмысленных звонков;
🔥 Корпоративную технику за счет компании;
🔥 Сотрудничество с командой высококвалифицированных специалистов, настоящих профессионалов своего дела;
🔥 Работу над масштабным, интересным и перспективным проектом.


Если в этом описании вы узнали  себя - пишите. Обсудим проект, задачи и все детали :)  
  
Павел,  
✍️TG: @pavel_hr  
📫 papolushkin.wanted@gmail.com`

var toTranslit = map[string]string{
	"а": "a",
	"б": "b",
	"в": "v",
	"г": "g",
	"д": "d",
	"е": "e",
	"ё": "yo",
	"ж": "zh",
	"з": "z",
	"и": "i",
	"й": "j",
	"к": "k",
	"л": "l",
	"м": "m",
	"н": "n",
	"о": "o",
	"п": "p",
	"р": "r",
	"с": "s",
	"т": "t",
	"у": "u",
	"ф": "f",
	"х": "h",
	"ц": "c",
	"ч": "ch",
	"ш": "sh",
	"щ": "sch",
	"ъ": "'",
	"ы": "y",
	"ь": "",
	"э": "e",
	"ю": "ju",
	"я": "ja",
}

func translitIt(s string, table map[string]string) string {
	out := ""
	for _, runa := range s {
		if table[string(unicode.ToLower(runa))] == "" {
			out += string(runa)
			continue
		}
		out += table[string(unicode.ToLower(runa))]
	}
	return out
}

func searchEverything(data string) int {
	var emojis = "[📫✍️🔥📌✅💼‍💻🏢💵🌏🙂👨‍💻]"
	reSymbols := regexp.MustCompile(`.`)
	matches1 := reSymbols.FindAllString(data, -1)

	reEmojis := regexp.MustCompile(emojis)
	matches2 := reEmojis.FindAllString(data, -1)
	fmt.Printf("Found emojis: %d\n", len(matches2))

	return len(matches2) + len(matches1)
}

func removeEmojis(data string) string {
	var emojis = "📫✍️🔥📌✅💼‍💻🏢💵🌏🙂👨‍💻"
	out := ""
	for _, i := range data {
		if strings.ContainsAny(string(i), emojis) {
			out += "=)"
			continue
		}
		out += string(i)
	}
	return out
}

func main() {

	fmt.Printf("Size of original text: %d\n\n", searchEverything(data))
	formatted := removeEmojis(data)
	fmt.Printf("Size of formatted text: %d\n\n", searchEverything(formatted))
	fmt.Printf("Size of translit original text: %d\n\n", searchEverything(translitIt(data, toTranslit)))
	fmt.Printf("Size of translit formatted text: %d\n\n", searchEverything(translitIt(formatted, toTranslit)))
	LatinByteCount := float32(len(translitIt(data, toTranslit)))
	CyrillicByteCount := float32(len(data))
	fmt.Println(LatinByteCount / CyrillicByteCount)
}
