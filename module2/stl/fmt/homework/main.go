package main

import (
	"fmt"
)

type Person struct {
	Name  string
	Age   int
	Money float64
}

func generateSelfStory(name string, age int, money float64) {
	fmt.Printf("Hello! My name is %s. I'm %d y.o. And I also have $%.2f in my wallet right now", name, age, money)
}

func main() {
	p := Person{Name: "Andy", Age: 18, Money: 10.0006}

	// вывод значений структуры
	fmt.Println("simple struct:", p)

	// вывод названий полей и их значений
	fmt.Printf("detailed struct: %+v\n", p)

	// вывод названий полей и их значений в виде инициализации
	fmt.Printf("Golang struct: %#v\n", p)

	generateSelfStory(p.Name, p.Age, p.Money)
}

//type point struct {
//	x, y int
//}
//
//func fmtPackage() {
//	p := point{1, 2}
//	fmt.Printf("%v\n", p)
//	fmt.Printf("%+v\n", p)
//	fmt.Printf("%#v\n", p)
//	fmt.Printf("%T\n", p)
//	fmt.Printf("%t\n", true)
//	fmt.Printf("%d\n", 123)
//	fmt.Printf("%b\n", 14)
//	fmt.Printf("%c\n", 33)
//	fmt.Printf("%x\n", 456)
//	fmt.Printf("%f\n", 78.9)
//	fmt.Printf("%e\n", 123400000.0)
//	fmt.Printf("%E\n", 123400000.0)
//	fmt.Printf("%s\n", "\"string\"")
//	fmt.Printf("%q\n", "\"string\"")
//	fmt.Printf("%x\n", "hex this")
//	fmt.Printf("%p\n", &p)
//	fmt.Printf("|%6d|%6d|\n", 12, 345)
//	fmt.Printf("|%6.2f|%6.2f|\n", 1.2, 3.45)
//	fmt.Printf("|%-6.2f|%-6.2f|\n", 1.2, 3.45)
//	fmt.Printf("|%6s|%6s|\n", "foo", "b")
//	//fmt.Printf("|%-6s|%-6s|\n", "foo", "b")
//}
