package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet\n",
		"чистый код лучше, чем заумный код\n",
		"ваш код станет наследием будущих программистов\n",
		"задумайтесь об этом\n",
	}

	var writer bytes.Buffer

	for _, d := range data {
		n, err := writer.Write([]byte(d))
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if n != len(d) {
			fmt.Println("failed to write data")
			os.Exit(1)
		}
	}

	file, err := os.Create("./example.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	if _, err := io.Copy(file, &writer); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	file, err = os.Open("./example.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()
	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Println(err)
				os.Exit(1)
			}
		}
		fmt.Println(line)
	}
}
